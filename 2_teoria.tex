
% !TeX spellcheck = pl_PL
\chapter{Wprowadzenie teoretyczne}\label{chap:teoria}


\newcommand\specialsection{%
  \titleformat{\section}[runin]{\normalfont\Large\bfseries}{\thesection}{1em}{}[]
}
\newcommand\regularsection{%
  \titleformat{\section}{\normalfont\Large\bfseries}{\thesection}{1em}{}
}


\newcommand\specialsubsection{%
  \titleformat{\subsection}[runin]{\normalfont\normalsize\bfseries}{\thesubsection}{1em}{}[]
}
\newcommand\regularsubsection{%
  \titleformat{\subsection}{\normalfont\normalsize\bfseries}{\thesubsection}{1em}{}
}

\specialsection
\section{Język programowania Haskell}\hspace{-0.75em}{\normalfont\Large\bfseries\footnote{Opracowanie powstało w oparciu o \cite{presentation} i \cite{haskellwiki}}}
\regularsection
\vspace{\baselineskip}


\noindent
Haskell -- standaryzowany, czysto funkcyjny język programowania ogólnego
zastosowania z nieścisłą semantyką
i silnym typowaniem statycznym, który powstał
w~1990 roku i został nazwany na cześć Haskella Curry'ego. Pewną rzadkością wśród
języków programowania jest czysto funkcyjny charakter Haskella.
%
% Nieścisła semantyka oznacza, że jeżeli da się wyznaczyć wynik jakiegoś wyrażenia,
% to jest ono ewaluowane nawet, jeśli zawiera ono podwyrażenie niezdefiniowane lub nieobliczalne w sensie Turinga.
%
% Nieścisła semantyka oznacza, że składnia języka stanowi uniwersalne narzędzie,
% a programista ma pełną dowolność w nadawaniu jej zastosowań i budowaniu
% w oparciu o~nią działających rozwiązań.
%\js{Pierwsze słyszę.  Skąd jest wzięta ta definicja nieścisłej semantyki?
%  Zwracam również uwagę, że stwierdzenie o ``pełnej dowolności w nadawaniu
%  [składni] zastosowań'' jest bardzo nieprecyzyjne i co najmniej wątpliwe.  Agda
%  albo Coq na pewno umożliwiają poszerzanie składni w większym zakresie niż 
%  Haskell.  Można argumentować, że mechanizm quasi-quoting pozwala na dowolne
%  rozszerzenie składni, ale stanowi on raczej rzadko wykorzystywany element
%  Haskella i na pewno nie zasługuje na wzmiankę jako jedna z najważniejszych
%  cech języka.}
Warto zwrócić uwagę,
iż mimo założeń języków ,,czysto funkcyjnych'', Haskell pozwala na
efekty uboczne -- w przeciwnym wypadku nie byłoby możliwości nawet wypisywania danych.
Poprzez zastosowanie odpowiednich typów są one jednak jasno
oddzielone od obliczeń czysto funkcyjnych, w obrębie których efekty
uboczne faktycznie nie są dozwolone.

Kolejną ważną cechą tego języka jest leniwe wartościowanie (,,non-strict semantics'',
,,lazy evaluation'' albo ,,call-by-need semantics'').
Ta własność oznacza, że wartość wyrażenia nie jest obliczana, dopóki nie jest
potrzebna. Takie zachowanie pozwala, na przykład, definiować nieskończenie długą
listę deklarowaną przy pomocy niekończącej się rekurencji. W wyniku takiej definicji
wyliczone zostaną tylko te elementy listy, które okażą się być potrzebne w ramach 
przebiegu programu. To
pozwala na wiele bardzo eleganckich sposobów rozwiązania różnych
problemów. Dobrym przykładem może być definiowanie listy możliwych rozwiązań, po
którym następuje jej filtrowanie. Podczas filtrowania możemy usunąć rozwiązania
niedopuszczalne. W rezultacie, pozostała lista będzie zawierać tylko i wyłącznie
rozwiązania dopuszczalne. Dzięki leniwemu wartościowaniu ta operacja jest bardzo
prosta w implementacji oraz czytelna podczas analizy kodu. Jeżeli potrzebujemy 
tylko pierwszego rozwiązania,
możemy odczytać je z listy, mając gwarancję iż nic nie zostanie policzone
niepotrzebnie.


Kolejną konsekwencją leniwego wartościowania (bądź ,,nieścisłej semantyki'') jest to, że
jeśli da się wyznaczyć wynik jakiegoś wyrażenia,
to jest ono ewaluowane nawet, jeśli zawiera ono podwyrażenie niezdefiniowane lub nieobliczalne w sensie Turinga.

Z innej strony, w przypadku leniwego wartościowania trudno przewidzieć ilość
wykorzystywanej pamięci. Może to skutkować powstaniem trudnych do analitycznego
rozstrzygnięcia wątpliwości podczas analizy złożoności rozwiązania niektórych
problemów.

Kluczową własnością Haskella jest także zastosowanie silnego typowania. To, na
przykład, oznacza że niemożliwa jest przypadkowa konwersja typów danych. Takie
zachowanie z jednej strony prowadzi do zmniejszenia ilości powstających błędów,
lecz z~innej -- może ono być nieco kłopotliwe podczas pisania programów. Warto
jednak zaznaczyć, że dla programisty mającego choćby odrobinę doświadczenia w
programowaniu funkcyjnym takie zdarzenia nie występują na tyle często, by uważać
to za poważny problem.

W przeciwieństwie do wielu języków z silnym typowaniem, typy w Haskellu są
automatycznie rekonstruowane przez kompilator (wnioskowane na podstawie kontekstu).
Oznacza to że bardzo rzadko musimy
deklarować typ funkcji. Jednak dla zrozumiałości kodu zalecane jest deklarowanie
typów.

W typowych zastosowaniach języka Haskell można wyróżnić następujące konstrukcje
językowe:

\begin{itemize}
\item typy proste (\emph{atomic}) -- najprostsze typy danych, analogiczne do typów znanych z większości
popularnych języków programowania:
\begin{lstlisting}[language=haskell]
Int
Float
Bool
Char
\end{lstlisting}

\item typy o złożonej strukturze (na przykład listy, krotki i inne):
\begin{lstlisting}[language=haskell]
[Integer]
(Bool, Char)
Char -> Int
\end{lstlisting}

\item typy polimorficzne, których różne realizacje dla konkretnych typów opisane są 
wspólnym, abstrakcyjnym wyrażeniem:
\begin{lstlisting}[language=haskell]
[a]
a -> a
(a, Int)
a -> b
\end{lstlisting}

\item bezargumentowe konstruktory typów:
\begin{lstlisting}[language=haskell]
data Kolor = Czerwony | Zielony | Niebieski
Czerwony :: Kolor
\end{lstlisting}

\noindent operator \emph{::} oznacza posiadanie wskazanego typu.

\item konstruktory typów z argumentami:
%\js{j.w.}
\begin{lstlisting}[language=haskell]
data Punkt a = Pkt a a
Pkt :: a -> a -> Pkt a
Pkt 0.0 1.0 :: Pkt Float
\end{lstlisting}

\item typy rekurencyjne:
\begin{lstlisting}[language=haskell]
data Drzewo = Lisc a | Galaz (Drzewo a) (Drzewo a)
Galaz                   :: Drzewo a -> Drzewo a -> Drzewo a
Lisc Czerwony                :: Drzewo Kolor
Galaz (Lisc 7) (Lisc 3)     :: Drzewo Int
\end{lstlisting}

\item lambda-abstrakcje:
\begin{lstlisting}[language=haskell]
suma = \x -> \y -> x  + y
\end{lstlisting}

\item dopasowywania wzorców:
\begin{lstlisting}[language=haskell]
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n - 1)  + fib (n - 2)
\end{lstlisting}

\item funkcje składane zapisywane przy użyciu ,,strażników'', działających na zasadzie
podobnej do dopasowywania wzorców (z taką różnicą, że w przypadku strażników
wybierana jest pierwsza taka gałąź, której warunek jest spełniony):
\begin{lstlisting}[language=haskell]
sign x | x >  0  =   1
       | x == 0  =   0
       | x <  0  =  -1
\end{lstlisting}

\item wieloznaczności (\emph{wildcards}) -- dopasowywanie wartości które nie mają
wpływu na wynik:
\begin{lstlisting}[language=haskell]
head (x:_) = x
\end{lstlisting}
	
\end{itemize}

Zastosowanie języka Haskell ułatwia tworzenie oprogramowania, w którym jest mało
błędów. Pierwszym powodem tego jest fakt, że o ile Haskell jest językiem czysto
funkcyjnym (więc nie ma zachowań niezdefiniowanych ani -- poza ściśle określonymi sytuacjami -- efektów ubocznych).
Zachowanie niezdefiniowane oznacza, że program został napisany źle
i w zależności od architektury bądź początkowego stanu pamięci mogą wystąpić różne rezultaty, bo pozwala na to definicja języka. Proste przykłady w języku C++:

\begin{lstlisting}
char *pointer = new char[1];
char x = pointer[2]; // dostęp poza zaalokowany obszar
\end{lstlisting}

\begin{lstlisting}
char* pobierz_napis()
{
   char napis[100];
   strcpy(napis, "napis");
   return napis;
}

char *pointer = pobierz_napis();
// czas życia zmiennej napis wskazywanej przez pointer minął
printf("%s\n", pointer);
\end{lstlisting}

\noindent W takich przypadkach nie wiemy co dokładnie się wydarzy.

W przypadku Haskella wprost ze składni języka da się wywieść, jaki wynik da każda operacja -- 
i~wynik ten nie zależy od architektury komputera ani początkowego stanu
pamięci. Zapisy, które potencjalnie łamałyby tę zasadę skutkują błędem kompilacji.
Język Haskell jest przejrzysty referencyjnie, co oznacza, że każde 
wywołanie funkcji (przy czym w języku Haskell każde wyrażenie jest 
wywołaniem funkcji) dla ustalonych argumentów mogłoby zostać zastąpione konkretną wartością.
% \js{To brzmi tak, jak byśmy wywołując funkcje w Haskellu nie znali ich argumentów.}
Przejrzystość referencyjna wymaga, aby 
oddzielne wywołania tej samej funkcji z tymi samymi argumentami 
za każdym razem dawały ten sam wynik. Można to podsumować 
stwierdzeniem, że wszystkie wyrażenia są deterministyczne. 
Własność ta ułatwia dowodzenie poprawności programu oraz umożliwia
zastosowanie optymalizacji kodu poprzez spamiętywanie wyników, eliminację
powtarzających się fragmentów wyrażeń, leniwe wartościowanie a nawet
automatyczne zrównoleglenie obliczeń.
Kolejna zaleta wynika z tego, że
Haskell stosuje typowanie silne i nie zachodzą niejawne rzutowania typów --
a w językach nie mających tej własności znaczna część błędów powstaje na skutek
nieprzemyślanego zastosowania tego właśnie mechanizmu. Haskell jest językiem
wysokiego poziomu, co oznacza że programy często wyglądają jak opis
algorytmu.

Nie sposób nie dostrzec analogii do programowania deklaratywnego \cite{declarative}.
Programowanie deklaratywne często definiowane jako dowolny styl programowania
który nie jest imperatywnym. Istnieje kilka ogólnych definicji:

\begin{itemize}
\item program który opisuje \emph{jakie} obliczenia mają zostać wykonane a 
nie \emph{jak} to wyliczyć;
\item dowolny język programowania bez efektów ubocznych (lub dokładnie, 
przejrzysty referencyjnie);
\item język z jasnym związkiem z logiką matematyczną.
\end{itemize}

Te wszystkie cechy są podobne do cech Haskella.
Programista posługuje się raczej konceptami, co do których można
bezpiecznie założyć, że są reprezentowane poprawnie -- przy rozwiązywaniu
abstrakcyjnych problemów zredukowana jest potrzeba rozważań na poziomie
sprzętu. Pisząc funkcje na wyższym poziomie abstrakcji wiele szczegółów
technicznych pozostawiamy do rozwiązania kompilatorowi, na skutek
ryzyko wystąpienia błędów jest mniejsze, niż w językach niższego poziomu.
Kolejnym argumentem za deklaratywnym charakterem programowania w Haskellu jest to, że kolejność zapisu
obliczeń w pliku źródłowym nie ma związku z kolejnością w jakiej obliczenia będą wykonywane
(co wynika również z leniwego wartościowania).

\section{Glasgow Haskell Compiler}

Glasgow Haskell Compiler (znany także jako ,,The Glorious Glasgow Haskell
Compilation System'' albo po prostu ,,GHC'') -- natywny kompilator Haskella z otwartym kodem źródłowym,
napisany w języku Haskell. Zapewnia on
wieloplatformowe środowisko do pisania i testowania kodu w języku
Haskell. Głównymi autorami projektu są Simon Peyton Jones oraz Simon Marlow.

Kompilator to system, który przyjmuje na wejściu reprezentację programu w~pewnym
języku źródłowym i zwraca na wyjściu
równoważny program w innym języku. Językiem docelowym zwykle jest kod
wykonywalny, ale może to być też inny język programowania. Pod
względem architektury w kompilatorach można wyróżnić \emph{frontend}, dokonujący
przetworzenia programu wejściowego i \emph{backend}, generujący kod
docelowy. Podział ten umożliwia utrzymywanie wielu frontendów i backendów. W
Glasgow Haskell Compiler dostępne są trzy backendy:

\begin{itemize}
\item generujący kod maszynowy;
\item generujący kod w C;
\item generujący kod w języku pośrednim LLVM.
\end{itemize}

\specialsection
\section{Hoopl}\hspace{-0.75em}{\normalfont\Large\bfseries\footnote{Streszczenie oparte o \cite{hoopl}}}
\vspace{\baselineskip}
\regularsection

Jak zostało to wspomniane we wcześniejszej części pracy, Hoopl jest modularną
biblioteką wielokrotnego użytku do analizy przepływu danych oraz
transformacji. Ma ona następujące charakterystyczne cechy:

\begin{itemize}
\item Hoopl jest biblioteką czysto funkcyjną. Warto zauważyć, że czysto funkcyjne
  języki nie są przeznaczone do pisania standardowych algorytmów do
  transformacji grafów sterowania przepływem, ponieważ algorytmy grafowe z reguły
  polegają na licznych zmianach wejściowego stanu pamięci. W programowaniu funkcyjnym
  można to zrealizować chociażby za pomocą monad (w najbardziej klasycznym ujęciu
  byłyby to monady stanu), lecz rzadko taki wybór narzędzia jest w tym wypadku
  wygodny. Kolejny problem polega na tym, że w Haskellu nie ma mutowalności (możliwości zmiany stanu pamięci), o którą
  są oparte klasyczne algorytmy, a więc trzeba stosować algorytmy dostosowane 
  do niemutowalnych struktur danych.
  Czyni to omawiany przypadek wyjątkowym, gdyż czysto funkcyjny kod okazał się
  być nie tylko prosty do napisania, lecz także o wiele bardziej użyteczny, niż byłoby
  podejście specjalnie dostosowane do tego zagadnienia.
\item Hoopl jest biblioteką polimorficzną (wyjaśnienie poniżej).
\item Biorąc pod uwagę, że wśród danych przetwarzanych przez bibliotekę mogą
  pojawiać się bardzo lokalne rozumowania (na przykład: ,,y jest zdefiniowany
  przed x:=y+2''), analiza i transformacje przeprowadzane przez Hoopl dotyczą
  często małych i prostych wyrażeń i zależności.
  Struktura programu reprezentowana jako złożenie wielu małych wyrażeń
  może zostać przekształcona tak, aby uzyskać optymizację.
%\item Hoopl wprowadza subtelne algorytmy, w tym (a) przeplataną analizę i
%  przepisywanie, (b) spekulatywne przepisywanie, (c) obliczenie punktów 
%  stałych oraz (d) dynamiczną izolacje usterek.
\end{itemize}

Hoopl to biblioteka, która w prosty sposób pozwala na przeprowadzanie analizy przepływu 
danych i dokonywanie transformacji związanych z tą analizą na grafach kontroli 
przepływu. Przetwarzany graf z kolei składa się z mniejszych jednostek. Hierarchia
stosowanej w bibliotece struktury jest następująca:

\begin{itemize}
\item \emph{węzeł}, który jest definiowany przez klienta;
\item \emph{blok}, który jest sekwencją węzłów;
\item \emph{graf} -- bloki połączone krawędziami.
\end{itemize}

Przed omówieniem warto dodać, że w Hooplu \emph{węzły}, \emph{bloki} i \emph{grafy}
dzielą ze sobą istotną dla nas własność: \emph{kształt}. \emph{Kształt} jest własnością, która determinuje, czy 
element jest \emph{otwarty/zamknięty} na \emph{wejściu/wyjściu}.

\subsection{Węzły}

Najbardziej podstawowymi elementami grafu przepływu są \emph{węzły}. Mogą one
reprezentować instrukcje maszynowe (wywołania, przejścia) bądź (w ujęciu
wysokopoziomowym) proste wyrażenia. Reprezentacja grafu w Hooplu jest 
\emph{polimorficzna co do typu węzłów}, a więc każdy klient może definiować
\emph{węzły} według własnych założeń. Przykładowo:

\begin{lstlisting}[language=Haskell]
data Node e x where
  Label  :: Label       -> Node C O
  Assign :: Var -> Expr -> Node O O
  Branch :: Label       -> Node O C
  -- inne kostruktory
\end{lstlisting}

\emph{Węzły} zamknięte na wejściu są jedynie celami przekazania kontroli,
\emph{węzły} otwarte na wejściu i wyjściu nigdy nie wykonują przekazania
kontroli, a \emph{węzły} zamknięte na wejściu zawsze same dokonują przekazania 
kontroli.

\subsection{Bloki}

Hoopl grupuje \emph{węzły} klienta w \emph{bloki} i \emph{grafy}, które
w przeciwieństwie do \emph{węzłów} są już zdefiniowane w samym Hooplu:

\begin{lstlisting}[language=Haskell]
data O   -- Otwarty
data C   -- Zamknięty

data Block n e x where
  BlockCO  :: n C O -> Block n O O          -> Block n C O
  BlockCC  :: n C O -> Block n O O -> n O C -> Block n C C
  BlockOC  ::          Block n O O -> n O C -> Block n O C
  BNil    :: Block n O O
  BMiddle :: n O O                      -> Block n O O
  BCat    :: Block n O O -> Block n O O -> Block n O O
  BSnoc   :: Block n O O -> n O O       -> Block n O O
  BCons   :: n O O       -> Block n O O -> Block n O O  
\end{lstlisting}

Konstruktor \emph{BMiddle} tworzy \emph{blok} 
z jednym \emph{węzłem}. Jest to konstruktor polimorficzny pod
względem reprezentacji \emph{węzła}, ale monomorficzny w swoim kształcie.

Konstruktor \emph{BCat} łączy bloki ze sobą; jeżeli kontrola przechodzi z 
jednego bloku w inny, to sensownym jest ich złączenie w jeden. Dotyczy to przypadku,
gdy każdy blok jest otwarty w punkcie złączenia.
\js{Ale przy opisie węzłów napisał Pan, cytuję,: ``\emph{węzły} otwarte na
  wejściu i wyjściu nigdy nie wykonują przekazania kontroli''.  To jak BCat może
  łączyć otwarte węzły pomiędzy którymi przechodzi kontrola?}

\subsection{Grafy}

Hoopl składa bloki w grafy które są zdefiniowane następująco:

\begin{lstlisting}[language=Haskell]
type Graph = Graph' Block

data Graph' block (n :: * -> * -> *) e x where
GNil  :: Graph' block n O O
GUnit :: block n O O -> Graph' block n O O
GMany :: MaybeO e (block n O C)
      -> Body' block n
      -> MaybeO x (block n C O)
      -> Graph' block n e x
\end{lstlisting}

Podobnie jak \emph{bloki}, typ danych \emph{Graph} jest parametryzowany przez
\emph{wezły n} oraz przez ich \emph{kształt} na wejściu i wyjściu
(\emph{e} oraz \emph{x}). \emph{Graph} ma 3 konstruktory. \emph{GNil} 
reprezentuje graf pusty, zaś \emph{GUnit} -- \emph{graf} który zawiera w sobie
jeden \emph{blok}.

Bardziej uogólnione \emph{grafy} są reprezentowane konstruktorem \emph{GMany},
który zawiera trzy pola: opcjonalną sekwencje wejścia, ciało oraz 
opcjonalną sekwencje wyjścia.

Grafy mogą być złączone ze sobą ze złożonością $O(\log(n))$, gdzie $n$ 
to ilość \emph{zamkniętych bloków}. W przeciwieństwie do \emph{bloków},
\emph{grafy} mogą zostać złączone nie tylko w przypadku gdy obaj są
\emph{otwarte} w punkcie złączenia, ale i kiedy są \emph{zamknięte}:
%\js{Wydaje mi sie, że funkcja splice może być trudna do zrozumienia, jeśli się
%  jej nieco nie posprząta.  Proponuję wywalić adnotacje SCC, zastąpić operatory
%  \$! nawiasami i wyjaśnić czym jest JustO i NothingO. To ostatnie możnaby
%  zrobić już wcześniej przy omawianiu GMany. }

\begin{lstlisting}[language=Haskell]
gSplice :: NonLocal n => Graph n e a -> Graph n a x -> Graph n e x
gSplice = splice blockAppend

splice :: forall block n e a x . NonLocal (block n) =>
          (forall e x . block n e O -> block n O x -> block n e x)
       -> (Graph' block n e a -> Graph' block n a x -> Graph' block n e x)

splice bcat = sp
  where sp :: forall e a x .
              Graph' block n e a ->
              Graph' block n a x ->
              Graph' block n e x
    sp GNil g2 = g2
    sp g1 GNil = g1

    sp (GUnit b1) (GUnit b2) = GUnit (b1 `bcat` b2)

    sp (GUnit b) (GMany (JustO e) bs x) = GMany (JustO (b `bcat` e)) bs x

    sp (GMany e bs (JustO x)) (GUnit b2) = x `seq` GMany e bs (JustO x')
      where x' = x `bcat` b2

    sp (GMany e1 bs1 (JustO x1)) (GMany (JustO e2) b2 x2)
      = (GMany e1 (b1 `bodyUnion` b2)) x2
      where b1   = (addBlock (x1 `bcat` e2)) bs1

    sp (GMany e1 b1 NothingO) (GMany NothingO b2 x2)
      = (GMany e1 (b1 `bodyUnion` b2)) x2
\end{lstlisting}

\subsection{Krawędzie, etykiety i następniki}

Mimo tego, że Hoopl jest polimorficzny, to potrzebuje on informacji
o tym, jak kontrola może być przekazywana z jednego \emph{węzła} do
drugiego. Wewnątrz bloku \emph{krawędź} kontroli przepływu jest stosowana w
każdym wywołaniu konstruktora \emph{BCat}.
\js{A co z BSnoc i BCons?}
Ukryta krawędź wychodzi
z pierwszego lub środkowego węzła i ląduje w średnim lub ostatnim.

Pomiędzy blokami, \emph{krawędź} kontroli przepływu jest reprezentowana
w sposób okreśolny przez klienta. Wychodzi ona z ostatniego \emph{węzła} i wchodzi do
pierwszego. Ze względu na tę operację potrzebne jest, aby \emph{węzeł} był instancją Hooplowej
klasy typów \emph{NonLocal}:

\begin{lstlisting}[language=Haskell]
class NonLocal thing where 
  entryLabel :: thing C x -> Label
  successors :: thing e C -> [Label]
\end{lstlisting}

Metoda \emph{entryLabel} przyjmuje pierwszy \emph{węzeł} (zamknięty na 
wejściu) i zwraca jego \emph{etykietę} (\emph{Label}); metoda 
\emph{successors} natomiast przyjmuje ostatni (zamknięty na wyjściu)
\emph{węzeł} i zwraca etykiety do których można przekazać kontrolę.

Z tego wynika, że deklaracja \emph{węzła} po stronie klienta może być
postaci:

\begin{lstlisting}[language=Haskell]
instance NonLocal Node where
  entryLabel (Label l) = l
  successors (Branch b) = [b]
  successors (Cond e b1 b2) = [b1, b2]
\end{lstlisting}

Ponieważ klient udostępnia taką informacje o węzłach, wygodne z punktu widzenia
samej biblioteki jest udostępnianie takiej samej informacji dla bloków:

\begin{lstlisting}[language=Haskell]
instance NonLocal n => NonLocal (Block n) where
  entryLabel (BlockCO f _)   = entryLabel f
  entryLabel (BlockCC f _ _) = entryLabel f
 
  successors (BlockOC   _ n) = successors n
  successors (BlockCC _ _ n) = successors n
\end{lstlisting}

\section{Zagadnienie sieci przepływowych}
\js{Wydaje mi się, że cały ten podrozdział powinien być umieszczony przed opisem
  Hoopl.  Wtedy cały opis węzłów, bloków i grafów w hooplu będzie o wiele
  bardziej zrozumiały.}

\specialsubsection
\subsection{Podejście teoretyczne}\hspace{-0.5em}{\normalfont\normalsize\bfseries\footnote{Ten podrozdział jest wynikiem analizy \cite{dragonbook}}}
\vspace{\baselineskip}
\regularsubsection


Mimo tego, że język Haskell jest językiem funkcyjnym, nie można zapominać o
fakcie, że aktualnie na poziomie sprzętu istnieją tylko i wyłącznie języki
imperatywne, w wyniku czego programy funkcyjne muszą ulegać
przekształceniu. Przekształceniem funkcyjnego kodu źródłowego do 
imperatywnego w tym przypadku zajmuje się Glasgow Haskell Compiler.

Grafowa reprezentacja instrukcji nazywana jest grafem przepływu. Jest ona 
przydatna podczas przedstawiania algorytmów generacji kodu, nawet jeżeli graf
nie jest tworzony przez algorytm generacji kodu w sposób jawny. Wierzchołki w~grafie
przepływu symbolizują obliczenia, a krawędzie -- przypływ sterowania.

Blok bazowy jest sekwencją kolejnych instrukcji, do której sterowanie wchodzi 
na początku i z której wychodzi na końcu: bez możliwości zatrzymania lub rozgałęzienia
przed jej końcem. Blok ten obejmuje zbiór wyrażeń. Są to te wyrażenia, których
czas życia nie kończy się przy wyjściu z bloku.

Dla bloku bazowego można zastosować różne przekształcenia, które nie zmieniają
zbioru wyrażeń obliczanych przez blok. Celem wielu tych przekształceń jest
poprawienie jakości kodu, który ostatecznie zostanie wygenerowany.

Najważniejszymi przekształceniami zachowującymi strukturę są:

\begin{enumerate}
\item Usuwanie podwyrażeń wspólnych. Weźmy blok:
\begin{lstlisting}
a := b + c
b := a - d
c := b + c
d := a - d
\end{lstlisting}

Druga i czwarta instrukcja obliczają to samo wyrażenie: $b+c-d$,
więc można ten blok przekształcić do:

\begin{lstlisting}
a := b + c
b := a - d
c := b + c
d := b
\end{lstlisting}

Uwaga: mimo tego że 1 oraz 3 instrukcja na pierwszy rzut oka obliczają to
samo wyrażenie, to 2 instrukcja przedefiniowuje b, wobec czego $a \neq c$!

\item Usuwanie kodu martwego. Przypuśćmy że pewna zmienna jest martwa (nie
jest już nigdzie dalej używana), ale w bloku jest punkt zmiany wartości tej
zmiennej. Taką instrukcje można bezpiecznie usunąć.
\item Zmiana nazw zmiennych tymczasowych.
\item Zmiana kolejności instrukcji.
\end{enumerate}

Do przekształcenia zbioru wyrażeń obliczanych przez blok stosowane są również
przekształcenia algebraiczne. Do przykładu takie wyrażenia:

\begin{lstlisting}
x := x + 0
x := x * 1
\end{lstlisting}

\noindent można śmiało pominąć ponieważ nie skutkują żadnymi zmianami
wartości.

Optymizacje kompilatora muszą zachowywać funkcjonalność oryginalnego programu.
Pomijając pewne wyłączne przypadki, kiedy programista implementuje pewien
konkretny algorytm, kompilator nie jest w stanie wywnioskować wystarczająco
dużo o programie, aby przekształcić go na oparty na innej zasadzie działania, równoważny
i jednocześnie bardziej efektywny algorytm. Z poziomu kompilatora można jednak zastosować względnie niskopoziomowe
semantyczne transformacje, posługując się ogólnymi algebraicznymi 
\emph{faktami} (na przykład $i+0=i$) lub semantyką obliczeniową
(wykonywanie tej samej operacji na tych samych wartościach prowadzi do
tego samego wyniku).

Dobrym przykładem prostej optymizacji może być eliminacja martwego
kodu. Załóżmy że mamy:

\begin{lstlisting}
if (debug) print ...
\end{lstlisting}

\noindent w przypadku gdy mamy stałą:

\begin{lstlisting}
debug = false
\end{lstlisting}

\noindent to taki kod może zostać wyeliminowany, tak aby ewaluacja warunku w ogóle nie była wykonywana podczas uruchomienia. Ogólnie, podobne działania
określane są jako \emph{propagacja stałych} (mechanizm ten został dokładnie opisany w dalszej części pracy).

Innym prostym co do koncepcji przykładem optymizacji jest \emph{code motion}.
Załóżmy, że mamy:

\begin{lstlisting}
while (i <= limit-2) /* limit jest ewaluowany co iterację */
\end{lstlisting}

\noindent po przekształceniu dostaniemy ekwiwalent w postaci:

\begin{lstlisting}
t = limit - 2
while (i <= t) /* limit został zewaluowany tylko raz */
\end{lstlisting}

Teraz wyrażenie \emph{limit - 2} zostanie wyliczone tylko raz, przed
wejściem do pętli. Przed tą zmianą przeprowadzaliśmy $n + 1$ operacji
odejmowania \emph{limit - 2} (gdzie $n$ to liczba iteracji).

Wszystkie optymizacje zależą od \emph{analizy przepływu}. Pojęcie
\emph{analizy przepływu} odnosi się technik zbierania informacji o przepływie
danych w trakcie wykonywania programu. Do przykładu, aby zaimplementować
mechanizm globalnej eliminacji wspólnych podwyrażeń, potrzebne jest
ustalenie, czy dwa identyczne (tekstowo) wyrażenia dają tę samą wartość
wzdłuż każdej możliwej ścieżki wykonania programu. Na to i wiele innych 
ważnych pytań można odpowiedzieć właśnie na podstawie analizy przepływu danych.

Podczas analizy zachowania programu musimy uważać na wszystkie możliwe
\emph{ścieżki} w grafie przepływu, które mogą opisywać praktyczny przebieg wykonania programu. Wtedy 
wyciągamy (z możliwych stanów programy w każdym punkcie) informację
potrzebną do rozwiązania konkretnego problemu \emph{analizy przepływu danych},
który rozpatrujemy.

Graf przepływu budowany jest w oparciu o następujące spostrzeżenia na temat możliwych ścieżek wykonania:

\begin{itemize}
\item wewnątrz prostego bloku, punkt programu po instrukcji jest taki sam
jak punkt przed następną instrukcją;
\item jeżeli istnieje krawędź łącząca blok $B\\ $ z blokiem $E>$, to punkt
programu po ostatniej instrukcji $B_y$ może następować bezpośrednio po
punkcie przed pierwszym wyrażeniem $B_2$.
\end{itemize}

Więc możemy zdefiniować \emph{ścieżkę wykonywania} (lub \emph{ścieżkę})
z punktu $p_i$ do $p$ jako taką sekwencję punktów $p_i,p_2, ..., p$, że dla
$i =1, 2, ..., n - 1$:

\begin{enumerate}
\item $p_i$ to punkt bezpośrenio przed instrukcją a $p_{i+1}$ -- punkt od razu po tej instrukcji, albo
\item $p_i$ to koniec pewnego bloku, a $p_{i+1}$ -- początek jego następnika.
\end{enumerate}

Ogólnie, może istnieć nieskończenie wiele \emph{ścieżek wykonywania}
programu, i nie ma skończonego górnego limitu długości \emph{ścieżki
wykonywania}. Analizatory programu uogólniają wszystkie możliwe stany,
które mogą zajść w badanym punkcie za pomocą skończonego zbioru \emph{faktów}.

Nawet pozornie prosty program może mieć nieskończoną liczbę możliwych \emph{ścieżek
wykonywania}.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\linewidth]{img/prosty}
\caption{Przykład programu ilustrujący abstrakcję przepływu danych}
\label{fig:blok}
\end{figure}

Kolejnym ważnym punktem jest pojęcie kraty (\emph{Lattice}). W celu zrozumienia tego pojęcia
warto odnieść się od abstrakcyjnych definicji. Schemat analizy przepływu 
$(D,V,\land,F)$ w ujęciu formalnym składa się z:

\begin{enumerate}
\item Kierunku przepływu danych $D$ -- w przód lub w tył.
\item Półkraty, która zawiera dziedzinę wartości $V$ i operator spotkań $\land$.
\item Rodziny funkcji przekształceń $V \to V$, którą będziemy oznaczać jako $F$. Ta rodzina ma zawierać funkcje odpowiadające warunkom brzegowym, które są funkcjami stałego
przejścia dla specjalnych węzłów wejścia i wyjścia w dowolnym grafie
przepływu.
\end{enumerate}

\emph{Półkrata} to taka para $(V, \land)$, gdzie $V$ jest niepustym zbiorem, oraz
binarny operator spotkań $\land$ ma takie własności, że dla dowolnych $x,y,z \in V$:

\begin{enumerate}
\item $x\ \land\ x = x$ (spotkania są idempotentne);
\item $x\ \land\ y=y\ \land\ x$ (spotkania są przemienne);
\item $x\ \land\ (y\ \land\ z) = (x\ \land\ y)\ \land\ z$ (spotkania są łączne).
\end{enumerate}

Połkrata posiada \emph{element górny} (\emph{top}), $\top$ taki że:

$$\forall_{x \in V}~ \top\ \land\ x = x .$$

Opcjonalnie \emph{półkrata} może posiadać \emph{element dolny}, 
oznaczany jako $\bot$, taki że:

$$\forall_{x \in V}~ \bot \land\ x = \bot .$$

W prawdziwej \emph{kracie} istotne są dwie operacje na elementach dziedziny:
spotkanie $\land$, które już opisałem oraz \emph{łączenie}, oznaczane jako $\lor$, % $\top$ i $\bot$.
które w wyniku daje najmniejsze ograniczenie górne dwóch elementów (które zatem muszą istnieć w~\emph{kracie}).

Globalna analiza przepływu danych zajmuje się zbieraniem informacji o 
pewnych aspektach działania programu i kojarzenia ich ze stosownymi 
miejscami w~grafie przepływu. Informacje są uzyskiwane w wyniku 
konstruowania i rozwiązywania tzw. równań przepływu danych. 
Typowe równanie ma postać:

$$out[S] = get[S] \cup (in[S]\setminus kill[S])$$

\noindent co może być zrozumiane jako: ,,informacja na końcu instrukcji S
jest generowana wewnątrz tej instrukcji lub jest informacją ,,przychodzącą''
do instrukcji S i nie zniszczoną w trakcie wykonywania instrukcji S''.
Informacja dotyczy instrukcji lub bloku bazowego.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{img/blok}
\caption{Przykład bloku grafu przepływu}
\label{fig:blok}
\end{figure}


\subsubsection{Analiza przepływu danych w programach strukturalnych}
W programie strukturalnym o gramatyce zdefiniowanej poprzez poniższy przykład:

\begin{lstlisting}
S -> id := E | S; S | if E then S else S | do S while E
E -> id + id | id
\end{lstlisting}

$gen[S]$ i $kill[S]$ mają charakter atrybutów syntetyzowanych i ,,lokalny''
zasięg (można je bezpośrednio wyznaczyć znając zawartość instrukcji lub
bloku, zaś dla „wyższych” pięter drzewa rozbioru syntaktycznego metodą
,,bottom-up'' według podanych wcześniej zależności). Możemy więc traktować je jak ,,stałe''.

$in[S]$ mają charakter atrybutów dziedziczonych przy czym przyjmuje się, 
że $in[S_0] = \emptyset$, gdzie $S_0$ odpowiada całemu programowi.

$out[S]$ mają znowu charakter atrybutów syntezowanych, zależnych od 
$in[S]$. Oba zbiory dają się wyznaczyć przy wykorzystaniu wcześniej 
podanych zależności i~wcześniej obliczonych $gen[S]$ i $kill[S]$
metodą ,,top-down'' (obliczanie $in[S]$) i~ponownemu ,,bottom-up'' (obliczanie 
$out[S]$). W ten sposób wykorzystujemy znane metody związane z gramatykami
atrybutowymi.

\subsubsection{Iteracyjny algorytm dla zasięgu definicji}

\textit{Wejście}: Graf przepływu $G$, w którym dla każdego bloku $B$
obliczono zbiory $gen[B]$ i $kill[B]$

\textit{Wyjście}: zbiory $in[B]$ i $out[B]$ obliczone dla każdego bloku $B$

\begin{lstlisting}
for each block B do out[B]:=gen[B];
change:=true
while change do:
  begin
    change:=false;
      for each block B do:
      begin
        in[B]:= out[P];
        old_out:=out[B];
        out[B]:= gen[B] (in[B] \ kill[B]);
        if out[B] <> old_out then:
          change:=true;
      end
  end
\end{lstlisting}

Algorytm pracuje dopóki występują zmiany a górną granicą iteracji jest 
ilość węzłów.

\specialsubsection
\subsection{Podejście praktyczne}\hspace{-0.5em}{\normalfont\normalsize\bfseries\footnote{Ten podrozdział jest wynikiem opracowania \cite{cleanup}}}
\vspace{\baselineskip}
\regularsubsection

\subsubsection{Analiza sieci przepływowej w przód}

Jako prosty przykład analizy w przód można wykorzystać demonstrację 
propagacji stałych:

\begin{lstlisting}
Before         Facts          After
----------------{}------------------
x := 3+4                     x := 7
---------------{x=7}----------------
z := x > 5                   z := True
----------{x=7, z = True}-----------
if z                         goto L1
then goto L1
else goto L2
\end{lstlisting}

Stała propagacja przechodzi od góry do dołu. W tym przykładzie zaczynamy od
pustego faktu. Mając ten fakt i węzeł x := 3+4 dokonujemy transformacji: 
zamieniamy x := 3+4 w x := 7. Zatem, mając fakt x = 7 i węzeł z := x $>$ 5,
możemy transformować go do z := True. Ostatecznie, podczas analizy trzeciego
węzła eliminujemy mały kawałek martwego kodu:

\begin{lstlisting}
if z
  then goto L1
  else goto L2
\end{lstlisting}

\noindent zmieniając go w:

\begin{lstlisting}
goto L1
\end{lstlisting}

Analiza w przód rozpoczyna się od pewnej ustalonej startowej listy etykiet
wejścia (zwykle tylko jednej etykiety). Informację wejściową na temat każdej z~nich
reprezentujemy jako oddzielny \emph{fakt}.  Teoretycznie możliwe jest, że
domyślnie będzie to ,,dolny'' element analizy, ale dostarczenie takiego akurat
faktu do analizy w~przód jest raczej błędem programisty wywołującego funkcję.
\js{Stop.  To ostatnie zdanie jest powtórzeniem tego co z Simonem napislaiśmy na
  wiki, ale ja bym wolał mieć pewność że to jest prawda.  Przydałoby się pokazać
  przykład w który demonstruje działanie opisanej wcześniej teorii,
  tzn. elementów $\top$ i $\bot$ oraz operatorów spotkania i/lub złączenia.  I
  wtedy będzie widać, czy to w ogóle ma sens od strony teoretycznej.  Jakim
  operatorem łączymy fakty przy analize wprzód?  Bo jeśli łączylibyśmy je za
  pomocą operatora spotkania, to faktycznie przkeazanie jako faktu wejściowego
  $\bot$ byłoy bez sensu.}

Gdy zmienna nie należy do dziedziny odwzorowania to znaczy, że jest to odwzorowanie ,,do góry''
(co oznacza, że zmienna może posiadać różne wartości w~różnych ścieżkach
sterowania przepływu). Taka możliwość jest dopuszczalna, ale operacja łączenia
musi obsługiwać ,,Bot'' i związane z tym sytuacje. Tymczasem można zauważyć, ze
przy poprawnym zastosowaniu funkcji argument typu ,,Bot'' nie jest nigdy używany. To
spostrzeżenie jest kluczową podstawą do zmian proponowanych w ramach
niniejszej pracy.  Nie chcemy deklarować ani przekształcać argumentu, 
który w istocie nie
posiada możliwych prawidłowych zastosowań. Jego obecność w~bibliotece Hoopl
stanowi błąd w rozumowaniu jej twórców, który możliwy jest do naprawienia.
Jednak podczas omówienia proponowanych zmian Ning Wang (jedna z osób 
uczestniczących w podtrzymywaniu biblioteki) stwierdził, że da się napotkać
na sytuację, w której element dolny jest używany podczas analizy w przód.
Hipoteza ta pozostaje jednak nieudowodniona.

\subsubsection{Analiza sieci przepływowej w tył}

Klasycznie ujęte algorytmy analizy danych w tył na żywo dla każdego punktu
programu obliczają wartości tych zmiennych, które potencjalnie mogą być wczytywane przed
ich następnym przepisaniem.
\js{Obawiam się, że to pierwsze zdanie jest niezrozumiałe.}
Wynik zwykle jest wykorzystywany do eliminacji
martwego kodu, aby usunąć wyrażeń związanych ze zmiennymi, które w~przyszłości
nie zostaną użyte.

Stan wyjściowy bloku to zbiór zmiennych ,,żywych'' na końcu bloku
(które przydadzą się później, więc ich czas życia nie kończy się wraz z blokiem);
zaś jego stan wejściowy to zbiór zmiennych ,,żywych'' na początku jego wykonywania.
Stan wyjściowy tak naprawdę jest połączeniem stanów wejściowych następników.
Funkcja przenoszenia wyrażenia jest stosowana poprzez oznaczenie zmiennych,
które ulegają zmianie jako ,,martwych'', po czym oznaczaniu zmiennych, które są wczytywane
jako ,,żywych''.

Rozważmy następujący przykład:

\begin{lstlisting}
b1: a := 2
    b := 6
    d := 3
    z := 100

    if a > b then
b2:   k := a + b
      d := 2
b3: endif
    k := 4
    return b * d + k
\end{lstlisting}

Analiza w tył będzie miała następujący przebieg:

\begin{lstlisting}
---- in: {}
b1: a := 2
    b := 6
    d := 3
    z := 100  -- nieużywane
              -- {a, b, d} natomiast zostają
    if a > b then
---- out: {a, b, d} --- suma zbiorów wszystkich 'in' następników 

---- in: {a, b}
b2:   k := a + b
      d := 2
---- out: {b, d}

---- in: {a, b}
b3: endif
    k := 4
    return b * d + k
---- out: {}
\end{lstlisting}

Stan wejściowy \emph{b3} zawiera tylko $b$ i $d$, ponieważ $k$ jest zapisywane.
\js{Nie rozumiem: przecież na listingu oznaczono a i b jako stan wejściowy.  W
  ogóle ten przykład jest niejasny i przydałoby się go lepiej wytłumaczyć.}
Stan wyjściowy \emph{b1} jest sumą zbiorów stanów wejściowych \emph{b2}
oraz \emph{b3}. Definicja $z$ w \emph{b1} oraz definicja $c$ w \emph{b2} mogą
zostać usunięte.

W bibliotece Hoopl, wsteczna analiza (w zamkniętym na wejściu grafie) pobiera argument typu
\emph{(Fact x f)} dla grafu, gdzie x opisuje jego otwarty lub zamknięty stan 
na wyjściu.  Więc jeżeli x=O, to pomijany jest jeden fakt. \js{Jak to pomijany?} Takie podejście jest w
pełni uzasadnione, ponieważ ten fakt jedynie cofa nas do wyjścia w sieci
przepływowej. Jednak jeżeli x=C, to przekazujemy dalej argument typu
\emph{FactBase}. \js{Nie przypominam sobie, aby reprezentacja faktów w Hoopl
  została opisana.  To co jest tutaj napisane jest zupełnie niezrozumiałe
  dla czytelnika.}

Do analizy wstecznej potrzebny jest nam element dolny, który zostanie
wykorzystany do inicjalizacji pętli. Przykład:

\begin{lstlisting}
 L1: ...coś...
 CondBranch e L1 L2
 
 L2: coś
\end{lstlisting}

Aby przeprowadzić analizę wstecz dla L1, musimy połączyć fakty płynące
z~powrotem z L2 (które z kolei muszą zostać zbadane uprzednio) i L1. Podczas
pierwszej iteracji nie mamy żadnych faktów z L1. Można przypuszczać, że
moglibyśmy wykorzystać fakt z L2 i w niepożądany sposób pominąć połączenia z L1.
Na szczęście nigdy nie dotyczy to przypadku, gdy \emph{CondBranch} jest
bezwarunkowe. Daje nam to pewność, że nie ma żadnego innego faktu mogącego być
podstawą do połączenia.

Z tych rozważań wynika, że do analizy w tył przy wywołaniu funkcji element dolny 
powinien być przekazywany w jawny sposób.

%Wyniki autorskich badań przeprowadzonych w ramach niniejszej pracy a także
%spostrzeżenia zostaną zamieszczone w rozdziałach \ref{chap:nowa_teoria} oraz
%\ref{chap:badania}.

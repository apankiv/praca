% !TeX spellcheck = pl_PL
\chapter{Wstęp}\label{chap:wstep}

Tematem niniejszej pracy dyplomowej jest ,,Modyfikacja reprezentacji danych oraz
algorytmów analizy grafów w bibliotece Hoopl używanej przez Glasgow Haskell
Compiler''. Hoopl (\emph{Higher-order optimization library}) stanowi modularną
bibliotekę wielokrotnego użytku do analizy przepływu danych oraz ich
transformacji.~\cite{hoopl} Biblioteka pod względem organizacji projektu Glasgow 
Haskell Compiler jest
modułem niezależnym, jednak w wyniku zmian nastąpią również zmiany w samym
kompilatorze. Będą one dotyczyły modułów wykorzystujących bibliotekę Hoopl, takich
jak \emph{compiler/cmm/Hoopl/Dataflow.hs}.
Nie spowoduje to jednak żadnych zmian ani w kodzie wynikowym generowanym przez 
kompilator, ani zmian dla użytkownika.

Dziedziną tej pracy jest informatyka, w szczególności budowa kompilatorów oraz
proces analizy grafów przepływu danych.  Przedmiotem pracy jest usprawnienie
zastosowanych w wybranym rozwiązaniu algorytmów analizy grafów oraz modyfikacja
w tym celu niektórych struktur danych.

Praca jest poświęcona etapowi analizy grafu przepływu mającemu miejsce podczas
procesu kompilacji.  Szczególna uwaga zostanie poświęcona aktualnie istniejącym
mechanizmom oraz metodom postępowania wchodzącym w skład tej analizy.  Podczas
badań należało wykryć niedoskonałości merytoryczne i wynikające z~nich
problemy oraz przeprowadzić analizę i uzasadnienie możliwych rozwiązań.
Kluczowe jest dążenie do tego, aby w ramach przedstawionych rozwiązań wprowadzić
możliwie jak najwięcej usprawnień. 
Ponadto proponowane zmiany mają uprościć zrozumienie kodu źródłowego biblioteki, 
jego analizę oraz dalsze prace nad nim.

\section{Cele pracy}\label{sec:cele_pracy}
Głównymi celami niniejszej pracy są:

\begin{itemize}
\item analiza rozwiązań aktualnie stosowanych w Glasgow Haskell Compiler oraz
  bibliotece Hoopl;
\item analiza rozwiązań proponowanych przez społeczność GHC;
\item ustalenie niezbędnych zmian w bibliotece (szczególnie w zakresie samej jej
  zasady działania);
\item określenie planu dalszego wprowadzania zmian.
\end{itemize}

Podstawowym problemem z którego wynika cel pracy jest to, że aktualnie
biblioteka operuje na dość dużej ilości struktur danych, które w praktyce
nie są wykorzystywane.
Istota tego problemu zostanie wyjaśniona w dalszej części pracy.

Przy projektowaniu tego rodzaju usprawnień nie warto ignorować również faktu, że
o ile te dane są dość długo wewnątrz biblioteki, to niekoniecznie można
powiedzieć, że są zbędne. Konieczne jest zastanowienie się, czy choćby
teoretycznie istnieją przypadki, w których mogłyby one być wykorzystywane. Takie
rozumowanie wymaga dokładniejszej analizy oraz omówienia bezpośrednio w
gronie twórców biblioteki oraz kompilatora.

Kolejnym, mniej przełomowym lecz bardzo ważnym celem pracy jest zapoznanie się z
paradygmatem programowania funkcyjnego, funkcyjnym językiem programowania
Haskell, strukturą Glasgow Haskell Compiler oraz metodami analizy grafów
przepływu danych.

\section{Przegląd literatury oraz uzasadnienie wyboru tematu}

Wybór tematu doskonale wpisuje się zarówno w studia na specjalności IOAD jak i w
moją osobistą działalność w ramach Sekcji Programowania Funkcyjnego Koła
Naukowego. Dodatkowym argumentem do podejmowania takiej a nie innej działalności
było to, że paradygmat funkcyjny (jak i język Haskell) nie jest szeroko
przedstawiony w trakcie studiów, a więc był nowością dla mnie i stanowi element
ponadprogramowy.

W obliczu powyższego, podejmowanie pracy w języku funkcyjnym było ciekawym
wyzwaniem. To bardzo dobra okazja aby rozwinąć swoje umiejętności w~zakresie
zdobywającego coraz większą popularność języka programowania. Zważywszy, że
główne cele pracy związane są z biblioteką stanowiącą część istniejącego kompilatora, w ramach
niniejszej pracy miałem okazję od strony praktycznej wykonać zadanie związane z działaniem
kompilatorów. Wybrane zagadnienie jest oczywiście dalece węższe, niż
pisanie kompletnego kompilatora, ale operowanie na kodzie odpowiedzialnym
za optymalizację świadczy o potencjale wybranego tematu. Wybór takiego zagadnienia od razu 
zapewnia podstawy do dalszego zagłębiania się w tę zaawansowaną tematykę w ramach planowanych 
przyszłych działań, które będą już wykraczały poza zakres pracy.
Doświadczenia związane z tworzeniem niniejszej pracy były na tyle inspirujące, 
że znacznie poszerzyły one moje ogólne spojrzenie na programowanie.

Kolejnym wyzwaniem był wysoki poziom odpowiedzialności podczas nawet
najmniejszych zmian. Aby zostały one przyjęte przez liczną społeczność twórców
Glasgow Haskell Compiler oraz (szczególnie) przez twórców biblioteki, wszystkie
zmiany musiały być poprawne oraz uzasadnione na podstawie głębokiej analizy
problemu. Dodatkowa korzyść związana z opracowywaniem takiego właśnie tematu
wiąże się z tym, że wcześniej nie miałem okazji współpracować z innymi
programistami w~ramach projektu o otwartym kodzie źródłówym.

Podczas pisania pracy posługiwałem się głównie dokumentacją Glasgow Haskell
Compiler zamieszczoną na oficjalnej stronie internetowej projektu
(\url{https://ghc.haskell.org/trac/ghc}) oraz teorią
budowania kompilatorów \cite{dragonbook}. Te źródła umożliwiły mi zrozumienie
aktualnego statusu oraz zasady działania badanego modułu oraz stosownych metod
postępowania w kierunku osiągnięcia celów głównych.

\section{Układ pracy}
W Rozdziale \ref{chap:teoria}
znajduje się opis teorii, która pomaga w zrozumieniu pracy i poszczególnych
etapów wprowadzanych zmian.  Rozdział \ref{chap:nowa_teoria} przedstawia własne
wyniki teoretyczne, które stanowią kluczowy rezultat przeprowadzonych badań.
Rozdział~\ref{chap:badania} przedstawia wyniki badań związane z częścią
praktyczną oraz opis problemów które pojawiły się w trakcie wykonania pracy,
stając się źródłem wartościowych wniosków.  Rozdział \ref{chap:podsumowanie}
podsumowuje uzyskane wyniki oraz przedstawia możliwe działania w kierunku
eliminacji napotkanych problemów i dalszego rozwoju biblioteki, który
wykraczałby poza zakres niniejszej pracy.
